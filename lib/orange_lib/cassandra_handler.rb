require 'cassandra'

module OrangeLib
  class CassandraHandler

    # Initialize a connection to cassandra
    # @param [String] host ip address of the cassandra host
    # @param [String]  key_space the key_space for connection to cassandra host
    # @return [Session] Cassandra session.
    # @example
    #     @__cassandra_handler ||= HMS::CassandraHandler.new('localhost', 'halo_test')
    def initialize(host, key_space)
      cluster = Cassandra.cluster(hosts: [host])
      @session  = cluster.connect(key_space)
    end

    # Execute a sql statement.
    # @param [String] sql_string the sql statement you would like to execute
    # @return [Cassandra::Result]
    # @example
    #     cassandra_handler ||= HMS::CassandraHandler.new('localhost', 'halo_test')
    #     sql = "SELECT count(*) FROM data WHERE uid in ('#{items_list}')"
    #     result = cassandra_handler.execute(sql)
    #     result.rows.each do |row|
    #     num_rows = row["count"]
    # end
    def execute(sql_string)
      @session.execute(sql_string)
    end
  end
end