require 'open3'
module OrangeLib
  module CommandLine

    # Execute a command line
    # @param [String] command command
    # @return [String] String of output which will be print on screen when we execute the comman
    # @example
    #     execute_command('echo $PATH')
    def execute_command(command)
      output = ''
      Open3.popen2e(command) {|stdin, stdout_and_stderr, wait_thr|
        stdout_and_stderr.each{|line|
          puts line
          output << line
        }
      }
      output
    end
  end
end
