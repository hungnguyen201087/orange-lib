require 'httparty'
require_relative 'vars'
require_relative 'command_line'

module OrangeLib
  class Error < StandardError; end
  class Rest
    include HTTParty
    include OrangeLib::Variables
    include OrangeLib::CommandLine

    attr_reader :response
    attr_reader :request

    def initialize()
    end

    def initialize(opts={})
      mergeOpt opts
    end

    def mergeOpt(opts={})
      self.class.default_options = self.class.default_options.merge(opts.to_hash)
      puts self.class.default_options
    end

    def perform_request(http_method, path, options={}, &block)  #:nodoc:
      method = eval("Net::HTTP::#{http_method.to_s.capitalize}")
      options = ModuleInheritableAttributes.hash_deep_dup(self.class.default_options).merge(options.to_hash)
      begin
        if (!options[:debug_request].nil? && options[:debug_request].to_s.downcase.eql?("true"))
          options = debugPass options
        end

        if (!options[:debug_request].nil? && options[:debug_request].to_s.downcase.eql?("false"))
          options = debugErr options
        end

        process_headers(options)
        process_cookies(options)
        puts options
        @response = HTTParty::Request.new(method, path, options).perform(&block)

      rescue => e
        puts "Rescued #{e.inspect}"
      end

      @response

    end

    def response
      raise OrangeLib::Error.new('@response is null') if @response.nil?
      @response
    end

    def process_headers(options)
      if options[:headers] && headers.any?
        options[:headers] = headers.merge(options[:headers])
      end
    end

    def process_cookies(options) #:nodoc:
      return unless options[:cookies] || self.class.default_cookies.any?
      options[:headers] ||= headers.dup
      options[:headers]['cookie'] = cookies.merge(options.delete(:cookies) || {}).to_cookie_string
    end

    def headers(h = {})
      raise ArgumentError, 'Headers must an object which responds to #to_hash' unless h.respond_to?(:to_hash)
      self.class.default_options[:headers] ||= {}
      self.class.default_options[:headers].merge!(h.to_hash)
    end

    def setHost(host)
      raise OrangeLib::Error.new('host param is null') if host.nil?
      self.class.base_uri host
    end

    def Host
      self.class.base_uri
    end

    def setFollowRedirect(flag)
      self.class.follow_redirects flag
    end

    def debugPass(opts={})
      raise ArgumentError, 'opts param must an object which responds to #to_hash' unless opts.respond_to?(:to_hash)
      self.class.default_options[:debug_output] = $stdout
      opts = ModuleInheritableAttributes.hash_deep_dup(self.class.default_options).merge(opts.to_hash)
      opts
    end

    def debugErr(opts={})
      raise ArgumentError, 'opts param must an object which responds to #to_hash' unless opts.respond_to?(:to_hash)
      self.class.default_options[:debug_output] = $stderr
      opts = ModuleInheritableAttributes.hash_deep_dup(self.class.default_options).merge(opts.to_hash)
      opts
    end
  end
end