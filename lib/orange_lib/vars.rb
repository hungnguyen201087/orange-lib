module OrangeLib
  module Variables

    # Create variable with input name and value. If input value is not passed, method will return value of variable
    #(This function is used if we would like to store a value to a variable in cucumber)
    # @param [String] name variable name
    # @param [Object] value value of variable
    # @return [Object] value of variable if input value is not passed.
    # @example
    #   variable('REPORT_ID', 'c73f5f2a-d098-4d84-8831-35c704773747') => store report id as variable REPORT_ID
    #   variable('REPORT_ID') => return value of variable REPORT_ID
    def variable(name, value = nil)
      name = name.to_sym
      return variables[name] if value.nil?

      variables[name] = value
    end


    # Remove all of stored variables.
    # @example
    #   clear_variables
    def clear_variables
      variables.clear
    end


    # Interpret an input string or hash by using the variable that set before that or the  params  argument
    # @params [String] input the template string.
    # @params [Hash] params the data is used for template
    # @example
    #   app.variable('app', 'bdd-lib')
    #   model = { name: 'Chris', friends: [{fname: 'Hung'}, {fname: 'Aurora'}] }
    #   template = 'Hello %{name} from %{app}, your friends are %{friends[0].fname} and %{friends[1].fname}'
    #   output = app.interpret_string(template, model) # -> Hello Chris from bdd-lib, your friends are Hung and Aurora
    def interpret_string(input, params = {})
      # Merge optional parameters with global variables
      params = variables.merge(params)
      params = flatten_hash(params)

      if input.class == Hash
        input.each { |k, v| input[k] = interpret_string(v) }
      elsif input.class == String && input =~ /(\%\{.+\})+/
        input % params
      else
        input
      end
    end

    private
    def variables
      @variables ||= {}
    end

    def flatten_hash(hash)
      hash.each_with_object({}) do |(key, value), output|
        if value.is_a? Hash
          flatten_hash(value).map do |v_key, v_value|
            output["#{key}.#{v_key}".to_sym] = v_value
          end
        elsif value.is_a? Array
          value.each_with_index do |item, index|
            v_hash = {"[#{index}]" => item}
            flatten_hash(v_hash).map do |v_key, v_value|
              output["#{key}#{v_key}".to_sym] = v_value
            end
          end
        else
          output[key.to_sym] = value.to_s
        end
      end
    end
  end
end
