# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'orange_lib/version'

Gem::Specification.new do |spec|
  spec.name          = 'orange_lib'
  spec.version       = OrangeLib::VERSION
  spec.authors       = ['Hung Thanh Nguyen']
  spec.email         = ['hungtnguyen@kms-technology.com']

  spec.summary       = 'Rest lib support to create cucumber step definitions'
  spec.description   = 'A set of rest lib utilizing HTTParty that ease basic testing of Restful APIs'
  spec.homepage      = 'https://rubygems.org/gems/orange'
  spec.license       = 'Aurora'

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    #spec.metadata['allowed_push_host'] = 'TODO: Set to 'http://mygemserver.com''
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.'
  end

  spec.files         = Dir['lib/**/*']
  spec.test_files   = Dir['features/**/*']
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'bundler', '~> 1.10'
  spec.add_dependency 'rake', '~> 10.0'
  spec.add_dependency 'rspec',       '~> 3.2'
  spec.add_dependency 'cucumber',    '~> 2.0'
  spec.add_dependency 'httmultiparty',    '~> 0.3'
  spec.add_dependency 'httparty',    '~> 0.13'
  spec.add_dependency 'jsonpath',    '~> 0.5'
  spec.add_dependency 'rack-test',   '~> 0.6'
  spec.add_dependency 'cassandra-driver', '>= 0.3', '>= 3.0.0.rc.1'
  spec.add_dependency 'rubysl-base64', '~> 2.0'
  spec.add_dependency 'nokogiri', '~> 1.6'
end
